---
title: "Syllabus"
date: 2018-08-25T14:09:23+03:00
draft: false
---


# Course Syllabus

**Credits:** 3 credit hours
**Prerequisite:** CPIT-204, CPIT-305

## Description
 
This course provides students with essential skills and hands-on experience on modern tools and technologies for administering development and deployment environments. This course discusses concepts essential to the administration of servers and introduces them into the recent technologies and infrastructures that run and manage modern software systems. Students who successfully complete this course will gain the essential knowledge and skills to work as a system administrator, DevOps engineer, or a site reliability engineer. This course uses UNIX and Linux operating systems to demonstrate various aspects of system administration. These operating systems powers over 94% of the world’s supercomputers and most of the servers powering the Internet. Students will gain a sufficient understanding of UNIX and Linux systems and be prepared to manage real world systems.

This course includes topics such as: configuring and deploying systems, troubleshooting system and network problems, shell scripting, virtualization, performance tracing and analysis, automation, and configuration management. 

## Course Learning Objectives
Upon successful completion of this course, students will be able to:

- Recognize the different administrative activities performed by a system administrator in an organization.
- Identify the structure of the UNIX file system and manage storage devices.
- Identify the components of a UNIX process and its life cycle.
- Demonstrate proficiency with the command line interface and common UNIX system management tools.
- Effectively plan, evaluate, perform, and schedule backup and recovery strategies.
- Effectively troubleshoot difficult and common system and networking problems.
- Effectively use virtualization technologies to utilize resources.
- Apply configuration management tools to automate system deployment and orchestrate changes across a number of machines.


## Textbook/References
- UNIX and Linux System Administration Handbook, 5th Edition,  Evi Nemeth, Garth Snyder, Trent R. Hein, Ben Whaley, Dan Mackin, Addison-Wesley Professional, August 18, 2017, ISBN-10: 0134277554, [website](https://admin.com/)
- Additional reading and video materials will be provided to students weekly.


## Course Outline

#### Module 1: Fundamentals of UNIX and LINUX systems
- Fundamentals of system administration: an overview
  - Essential roles of the system administrator.
  - History and background of UNIX and Linux systems.
  - UNIX and Linux Distributions
- Fundamentals of Unix and Linux systems
  - Installing, starting, and configuring software.
  - Shells and Command Line Interface (CLI) tools.
  - User account management
  - Backup and recovery
- The UNIX file system
  - The structure of the file system and its primitives
  - Storage management
  - Process management and scheduling.
- The Bash shell
  - Terminal navigation
  - File manipulation
  - Piping, streaming, and flow redirection
  - Searching and filtering
  - Program execution and package management
- The shell and shell scripting
  - Basic bash scripting
  - Python scripting

#### Module 2: Cloud Computing and App Deployment
- Cloud platform choices
  - Amazon Web Services, Google Cloud Platform, Microsoft Azure, and Digital Ocean.
  - Cloud service fundamentals: access, regions, virtual private server
  - Identity management and authorization
  - Cost control
- App deployment: setup and configuration
  - LAMP stack
  - Apache, PHP, and MySQL setup
  - Server configuration


#### Module 3: Networking and Virtualization
- Fundamentals of network adminstration
  - TCP/IP, DHCP configuration
  - Host configuration
  - VPN setup and configuration
  - Network troubleshooting
  - Firewall rules and configuration
  - Web hosting setup and configuration
- Virtualization
  - Native and cloud virtualization
  - Server Virtualization
  - Linux containers
  - Docker containers

#### Module 4: Performance Management
- Syslog and log files
  - Log management and rotation
  - Logging analysis and debugging
- Performance management
  - Performance metrics
  - Load balancing mechanisms
  - System performance analysis
- Performance tracing tools
  - Tracing sources
  - Performance profiling with perf, perf-tools, trace-cmd, and eBPF

#### Module 5: Software and configuration management
- Content management
  - Managing the software installed on the system for development and production environments
- Configuration management and automation
  - Configuration and scalability issues
  - Automation and configuration using Puppet
- Version Control
  - Git basics
  - Branching and merging
  - Remote repositories
  - Deploying and hosting a remote Git server

## Grading
- Labs: 20%
- Assignments: 40%
- Midterm exam: 20%
- In-class activities: 10%
- Group presentation: 10%
