---
title: "Lab 1"
date: 2018-09-01T14:12:36+03:00
draft: false
---
# Lab 1: Getting Started
> Install a Linux system, log in, and run simple commands using the shell.

## Objectives
1. Setting up a working Linux environment.
2. Logging in, activating the user interface, and logging out.
3. Changing user passwords
4. Reading manuals

## Setup
- Download and install Oracle VM [VirtualBox](https://www.virtualbox.org/), a free and open-source hypervisor.
- Download and install [CentOS](https://www.centos.org/)

## 1) Access the command line



## Lab excercises

1. Obtain the following results
  - The name and version of the Operating System.
  - The logged in user name
  - The host name
2. 