---
title: "Lab 2"
date: 2018-09-01T16:38:51+03:00
draft: true
---

## Lab 2

> The Linux file system. Copy, move, create, delete, and organize files from the bash shell prompt.