---
title: "Week 2"
date: 2018-09-12T07:25:35+03:00
draft: false
---
## Week 2: The Filesystem

> Learn the basic of files and the filesystem.

"On a UNIX/Linux system, almost everything you encounter is a file"

### 1. Navigating the Filesystem
The command `cd` is used to navigate to directories and `ls` is used to list the content of a directory.

**Example:**

```bash
$ pwd
/home/khalid
$ ls
note.txt todo.txt notes
$ ls -a
. .. note.txt todo.txt notes .hidden-file.txt
```

## 2. Creating directories
- Use the command `mkdir` to create directories:
- Use the command `mkdir` with the `-p` option to create parent directories as needed.

```bash
$ mkdir notes
$ mkdir -p notes/personal/summer
```

## 3. Creating and Deleting files

The command `touch` is used to create an empty file. Other tools can be used to create files (e.g., cp, text editors, etc.).

```bash
$ touch note.txt
$ touch .hidden-file.txt
```

The tool/command `rm` is used to delete files or directories. The tool `rmdir` is used to delete empty directories.
If the directory is empty, use the command `rmdir ./example-directory` or `rm -r ./example-directory` to delete directories and their contents recursively.

```bash
$ rm -r ./dir1/
$ rmdir 
```

## 4. Copying files
The command `cp` is used to copy files from source directory into the destination directory.

```bash
$ cp file_1 ./target
```

### 5. File Types
The command `ls -l' outputs a long listing format of a directory. The first character of each input line indicates the type of a file.

The three most common types are:

- **-** Regular file
- **d** Directory
- **l** A hard or soft link (i.e., a shortcut to another file on the system)

```bash
$ ls -l
total 3
drwxr-xr-x.   2 khalid  khalid  102 Sep  9 13:28 notes
-rw-rw-r-x.   1 khalid  khalid  102 Sep  9 14:29 note.txt
-rw-rw-r-x.   1 khalid  khalid  102 Sep  8 10:35 todo.txt
```

## 6. Soft links
A symbolic or "soft" link is a shortcut that points to a file on the system.

```bash
$ ln -s notes.txt my-shortcut-to-notes
```

## 7. Changing File Ownership and Group
In UNIX/Linux, every file is owned by a user and a group user. To find the username of the current logged in user, use the command `whoami` and to list the groups a user belongs to, use the command `groups` (e.g, `groups khalid`. 
The command `chown` is used to change both the owner of a file or its group (e.g., `chown username:groupname ./file-or-dir`). You may use the `-R` option to change the owner or group of a directory and its content recursively.

```bash
$ chown khalid:staff ./notes.txt
$ chown -R khalid:staff
```

## 8. File Security and Access Rights
The filesystem security model is based on permission bits to control access into files and directories. The access control model categorizes users into three different categories: owner, group, and others. Every file is owned by a user (called owner) and a group user. There's also a third types of users called _others_, for any user who is not the user owner and does not belong to the user group of the owner. For example, if we have a file owned by a user named _khalid_, and khalid belongs to a group called _staff_, then the access control (read, write, and execute) can be granted or denied to the owner _khalid_, the group _staff_, and everyone else (_others_).

The command to control the access to the file is called `chmod` and deals with only three bits called permission bits in the following order: read, write, and execute.

```bash
$ ls -l
-rw-r--r--   1 khalid  staff  545 Dec 20  2018 test.txt
```
The example above shows the owner, _khalid_, has read and write access, anyone in the group _staff_ can read the file, and everyone else or _others_ can read the file as well.

The permission bits can be specified in binaries or octal numbers:

![](/images/week-2/file-permission-bits.png)

We can grant all access to the owner, read access to members of the group, and deny access to others:

```bash
$ chmod 740 testfile.txt
```

### 9. Mounting an external storage
All external storage devices and partitions are attached to the system via a mount point using the `mount` command. To find all active partions on the system, use the command `df`. Next, create a directory to mount the content of the external storage and mount it on that directory.

Example:

```bash
$ df -h

$ mkdir -p /mnt/disks/mymainssd

$ mount /dev/sdab /mnt/disks/mymainssd

$ df -h

```

To unmount the external storage device, use the `umount`:

```bash
umount /mnt/disks/mymainssd
```

