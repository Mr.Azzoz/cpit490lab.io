---
title: "Intro"
date: 2018-09-13T06:33:20+03:00
draft: true
---

# Week 2: User and Program Management

## User Management
There're two types of user accounts: superuser or root user and standard users. The root user is a special user account used for system administration and has all the rights or permissions to make modifications in all modes.

It's advised that you avoid logging into the system as a root user for security reasons and to avoid serious damages caused by mistakes in entering commands. If a task requires root level permissions, then you can use the `sudo` command to run it as a root user. The `sudo` command requires the user who issued it to know the root password. 

## Creating a Sudo User
Not every standard user can issue the `sudo` command. To give a standard user a `sudo` access, we need to add the user to the `/etc/sudoers` file. This can be done indirectly by adding the user to the `wheel` group. This can be done by logging as a root user, and using the command `usermod -aG wheel <username>`.

```bash
# login as a root
$ su -
$ usermod -aG wheel khalid
```
You can test that the user has superuser privileges by by prepending `sudo` to a command that yrequires superuser privileges. For example, listing the contents of the `/root` directory.

```bash
$ sudo ls /root
```


## Listing all users on the system
The `/etc/passwd` file contains one line for each user account. Each line contains fields separated by a colon (:) symbol. The first field is the username.

```bash
$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
...
..
...
khalid:x:1000:1000:khalid:/home/khalid:/bin/bash
```

## Adding a new user

To add a user to the system, issue the command `useradd <username>`. This command should be issued as a root user or using `sudo`. Otherwise, you will get a Permission denied error.

```bash
$ sudo useradd ali
```

## Changing the password
To change the password of an existing user, use the command `passwd`:

```bash
$ sudo passwd ali
```

